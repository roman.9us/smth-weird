#include <iostream>
#include "database.h"

// Example of interactions without any interface(TBD)

int main() {
    // Create some db
    sqlite3* db = database::open_db("new_db");
    database::check_for_all_tables(db);
    database::add_new_user(db, "VASYA", "123");
    database::add_new_user(db, "KHAN", "1111");
    database::add_new_room(db, "214", 10);
    database::add_new_room(db, "215", 20);
    database::add_new_room(db, "216", 30);
    database::add_new_room(db, "217", 40);

    // Login as first user and book room
    int login = database::login(db, "VASYA", "123");
    database::request_free_list(db, "22::00","2020.05.15", 9);
    int room_id;
    std::cin >> room_id;
    database::book_room(db, room_id, login,"22::00","2020.05.15");

    // Then login as second and try to book for the same time
    login = database::login(db, "KHAN", "1111");
    database::request_free_list(db, "22::00","2020.05.15", 5);
    std::cin >> room_id;
    database::book_room(db, room_id, login,"22::00","2020.05.15");

    // Lets see the timetable
    database::show_timetable(db);

    database::close_db(db);
}
