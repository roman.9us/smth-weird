//
// Created by GEN on 15.04.2020.
//

#ifndef INC_1C_2020_DATABASE_H
#define INC_1C_2020_DATABASE_H

#include <sqlite3.h>
#include <string>

namespace database {
    int request_free_list(sqlite3* db, char* time, char* date, size_t seats_count);

    sqlite3* open_db(char* name);

    int close_db(sqlite3* db);

    int login(sqlite3* db, const std::string& login, const std::string& password);

    int add_new_user(sqlite3* db, const std::string& login, const std::string& password);

    int add_new_room(sqlite3* db, const std::string& name, size_t seats_count);

    int check_for_all_tables(sqlite3* db);

    int book_room(sqlite3* db, int room_id, int user_id, char* time, char* date);

    int show_timetable(sqlite3* db);

    int show_all_rooms(sqlite3* db);
};


#endif //INC_1C_2020_DATABASE_H
