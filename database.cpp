//
// Created by GEN on 15.04.2020.
//

#include "database.h"
#include <cstdlib>
#include <cstdio>

namespace database {
    const size_t MAX_REQUEST_SIZE = 4096;

    int execute_simple_sql_statement(char* sql, sqlite3* db, void* data, int (*callback)(void*,int,char**,char**)) {
        char* zErrMsg = nullptr;
        int rc;

        rc = sqlite3_exec(db, sql, callback, data, &zErrMsg);
        if( rc != SQLITE_OK ){
            fprintf(stderr, "SQL error: %s\n", zErrMsg);
            sqlite3_free(zErrMsg);
        }
        return 0;
    }

    static int request_free_list_callback(void* data, int argc, char** argv, char** azColName){
        for (size_t i = 0; i < argc; ++i){
            printf("%s == %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
        }
        printf("\n");
        return 0;
    }

    int request_free_list(sqlite3* db, char* time, char* date, size_t seats_count) {
        char sql[MAX_REQUEST_SIZE];
        snprintf(sql, MAX_REQUEST_SIZE,
                "SELECT * FROM rooms " \
                "WHERE seats_count > %ld " \
                "EXCEPT " \
                "SELECT r.id, r.name, r.seats_count FROM rooms AS r "\
                "LEFT JOIN timetable AS t ON r.id = t.room " \
                "WHERE t.time = '%s' AND t.date = '%s';",
                 seats_count, time, date);
        return execute_simple_sql_statement(sql, db, nullptr, request_free_list_callback);
    }

    sqlite3* open_db(char* name) {
        sqlite3* db;
        int rc;
        rc = sqlite3_open(name, &db);
        if ( rc ) {
            fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
            return nullptr;
        }
        return db;
    }

    int close_db(sqlite3* db) {
        sqlite3_close(db);
        return 0;
    }

    bool check_for_forbidden(const std::string& str) {
        return str.find(';') == std::string::npos;
    }

    int add_new_user(sqlite3* db, const std::string& login, const std::string& password) {
        if (!check_for_forbidden(login)) {
            fprintf(stderr, "Login contains forbidden characters\n");
            return 1;
        }

        std::hash<std::string> hash_fn;
        size_t hash = hash_fn(password);

        char sql[MAX_REQUEST_SIZE];
        snprintf(sql, MAX_REQUEST_SIZE,
                "INSERT INTO users(login, password) VALUES ('%s' , %lu);", login.c_str(), hash);
        return execute_simple_sql_statement(sql, db, nullptr, nullptr);
    }

    static int login_callback(void* data, int argc, char** argv, char** azColName){
        int* user_id = (int*)data;
        if (argc == 0) {
            *user_id = -1;
            return 0;
        }
        *user_id = strtol(argv[0], nullptr, 10);
        return 0;
    }

    int login(sqlite3* db, const std::string& login, const std::string& password) {
        int user_id = -1;

        if (!check_for_forbidden(login)) {
            fprintf(stderr, "Login contains forbidden characters\n");
            return 1;
        }

        std::hash<std::string> hash_fn;
        size_t hash = hash_fn(password);

        char sql[MAX_REQUEST_SIZE];
        snprintf(sql, MAX_REQUEST_SIZE,
                "SELECT id FROM users WHERE login = '%s' AND password =  %lu;", login.c_str(), hash);
        execute_simple_sql_statement(sql, db, &user_id, login_callback);
        return user_id;
    }

    int add_new_room(sqlite3* db, const std::string& name, size_t seats_count) {
        if (!check_for_forbidden(name)) {
            fprintf(stderr, "Name contains forbidden characters\n");
            return 1;
        }

        char sql[MAX_REQUEST_SIZE];
        snprintf(sql, MAX_REQUEST_SIZE,
                "INSERT INTO rooms(name, seats_count) VALUES ('%s' , %lu);", name.c_str(), seats_count);
        return execute_simple_sql_statement(sql, db, nullptr, nullptr);
    }

    int check_for_all_tables(sqlite3* db) {
        char sql[MAX_REQUEST_SIZE];
        snprintf(sql, MAX_REQUEST_SIZE,
                 "CREATE TABLE IF NOT EXISTS rooms("\
                "id INTEGER PRIMARY KEY AUTOINCREMENT,"\
                "name TEXT NOT NULL UNIQUE,"\
                "seats_count INTEGER NOT NULL);"\
                "CREATE TABLE IF NOT EXISTS users("\
                "id INTEGER PRIMARY KEY AUTOINCREMENT,"\
                "login TEXT NOT NULL UNIQUE,"\
                "password INTEGER NOT NULL);" \
                "CREATE TABLE IF NOT EXISTS timetable("\
                "user INTEGER,"\
                "room INTEGER,"\
                "time TEXT NOT NULL,"\
                "date TEXT NOT NULL,"\
                "FOREIGN KEY (user) REFERENCES users (id),"\
                "FOREIGN KEY (room) REFERENCES rooms (id),"\
                "UNIQUE(room, time, date));");
        return execute_simple_sql_statement(sql, db, nullptr, nullptr);
    }

    int book_room(sqlite3* db, int room_id, int user_id, char* time, char* date) {
        char sql[MAX_REQUEST_SIZE];
        snprintf(sql, MAX_REQUEST_SIZE,
                 "INSERT INTO timetable(user, room, time, date) VALUES (%d, %d, '%s', '%s');",
                 user_id, room_id, time, date);
        return execute_simple_sql_statement(sql, db, nullptr, nullptr);
    }

    static int show_timetable_callback(void* data, int argc, char** argv, char** azColName){
        for (size_t i = 0; i < argc; ++i){
            printf("%s == %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
        }
        printf("\n");
        return 0;
    }

    int show_timetable(sqlite3* db) {
        char sql[MAX_REQUEST_SIZE];
        snprintf(sql, MAX_REQUEST_SIZE,
                "SELECT u.login, r.name, t.time, t.date FROM timetable AS t "\
                "INNER JOIN users AS u ON u.id = t.user "\
                "INNER JOIN rooms AS r ON r.id = t.room");
        return execute_simple_sql_statement(sql, db, nullptr, show_timetable_callback);
    }

    static int show_all_rooms_callback(void* data, int argc, char** argv, char** azColName){
        for (size_t i = 0; i < argc; ++i){
            printf("%s == %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
        }
        printf("\n");
        return 0;
    }

    int show_all_rooms(sqlite3* db) {
        char sql[MAX_REQUEST_SIZE];
        snprintf(sql, MAX_REQUEST_SIZE,
                 "SELECT * FROM rooms");
        return execute_simple_sql_statement(sql, db, nullptr, show_all_rooms_callback);
    }
};